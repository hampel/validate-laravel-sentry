CHANGELOG
=========

1.2.0 (2013-12-17)
------------------

* added missing requires to composer.json, removed orchestra/testbench and added mockery
* rewrote service provider to use extend() rather than resolve() to add additional validation functions
* refactored Validator to avoid using Facades, use container from base Validator class instead
* changed service provider test to actually call the boot method (with partial mock of the service provider)

1.1.0 (2013-12-13)
------------------

* updated framework requirement to 4.1.*

1.0.0 (2013-09-27)
------------------

* initial release, based on hampel/validate-laravel-auth package
