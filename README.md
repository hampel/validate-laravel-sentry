Hampel Laravel Sentry Validator
===============================

Custom Validators for Laravel 4 for Sentry Authentication

By [Simon Hampel](http://hampelgroup.com/).

Installation
------------

The recommended way of installing Hampel Laravel Sentry Validator is through [Composer](http://getcomposer.org):

Require the package via Composer in your `composer.json`

	:::json
    {
        "require": {
            "hampel/validate-laravel-sentry": "1.1.*"
        }
    }

Run Composer to update the new requirement.

	:::bash
    $ composer update

The package is built to work with the Laravel 4 Framework.

Open your Laravel config file `config/app.php` and add the service provider in the `$providers` array:

	:::php
    "providers" => array(

        ...

        "Hampel\Validate\LaravelSentry\ValidateServiceProvider"

    ),

Note that we assume you are already using Cartalyst Sentry and thus have the `'Cartalyst\Sentry\SentryServiceProvider'` entry in
the providers array already.

Usage
-----

This package adds an additional validator for Laravel 4 - refer to [Laravel Documentation - Validation](http://laravel.com/docs/validation) for general usage instructions.

__sentry:*userid*__

The field under validation must be a password, which is combined with the supplied userid value which is then
passed to Sentry's User Provider `findByCredentials` routine to check whether this is a valid userid/password combination.

The parameter to the auth rule is required (if not provided, the rule will always fail), and should be set to the userid to
include in the credentials.

Create a user credentials array (userid, password), using the database column names for the array keys and pass that to the
validator. The array key of the userid can be accessed by the config setting `'cartalyst/sentry::users.login_attribute'`

Example
-------

	:::php
	$userid_field = Config::get('cartalyst/sentry::users.login_attribute');
	$password_field = "password";

	$userid = Input::get('email');
	$password = Input::get('password');

	$credentials = array(
		$userid_field => $userid,
		$password_field => $password
	);

	// Declare the rules for the form validation.
	$rules = array(
		$userid_field => 'required',
		$password_field => array('required', 'sentry:' . $userid)
	);

	// Validate the inputs.
	$validator = Validator::make($credentials, $rules);

	// Check if the form validates with success.
	if ($validator->passes())
	{
		dd("passes");
	}
	else
	{
		dd("fails");
	}
