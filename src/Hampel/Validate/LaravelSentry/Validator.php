<?php namespace Hampel\Validate\LaravelSentry;
/**
 * 
 */

use Cartalyst\Sentry\Users\UserNotFoundException;
use Cartalyst\Sentry\Users\WrongPasswordException;

class Validator extends \Illuminate\Validation\Validator
{
	public function validateSentry($attribute, $value, $parameters)
	{
		if (count($parameters) < 1) return false;

		$userid_field = $this->container['config']->get('cartalyst/sentry::users.login_attribute');
		$userid_value = $parameters[0];

		$password_field = $attribute;
		$password_value = $value;

		$credentials = array(
			$userid_field => $userid_value,
			$password_field => $password_value
		);

		try
		{
			$this->container->make('sentry.user')->findByCredentials($credentials);
			return true;
		}
		catch (\InvalidArgumentException $e)
		{
			return false;
		}
		catch (UserNotFoundException $e)
		{
			return false;
		}
		catch (WrongPasswordException $e)
		{
			return false;
		}

	}
}

?>
